import { Pipe, PipeTransform } from '@angular/core';
import { Shirt } from '../classes/shirt';

@Pipe({
  name: 'colourFilter'
})
export class ColourFilterPipe implements PipeTransform {

  transform(items: Shirt[], filter?: string): Shirt[] {

    if (filter != null && filter !== '') {
      return items.filter((shirt) => shirt.colour === filter);
    }

    return items;
  }

}
