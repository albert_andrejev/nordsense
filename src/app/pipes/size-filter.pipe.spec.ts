import { SizeFilterPipe } from './size-filter.pipe';
import { Shirt } from '../classes/shirt';

describe('SizeFilterPipe', () => {
  it('should return original list of items if filter absent', () => {
    const pipe = new SizeFilterPipe();

    const items: Shirt[] = [new Shirt, new Shirt, new Shirt];

    const result = pipe.transform(items);
    expect(result).toEqual(items);
  });

  it('should return original list of items if filter empty', () => {
    const pipe = new SizeFilterPipe();

    const items: Shirt[] = [new Shirt, new Shirt, new Shirt];

    const result = pipe.transform(items, '');
    expect(result).toEqual(items);
  });

  it('should return item with specified size only', () => {
    const pipe = new SizeFilterPipe();

    const shirt1: Shirt = <any>{
      size: 'L',
      id: 1,
      price: 1
    };
    const shirt2: Shirt = <any>{
      size: 'XL',
      id: 1,
      price: 1
    };

    const items: Shirt[] = [shirt1, shirt2, new Shirt];

    const result = pipe.transform(items, 'L');
    expect(result[0]).toEqual(shirt1);
  });
});
