import { Pipe, PipeTransform } from '@angular/core';
import { Shirt } from '../classes/shirt';

@Pipe({
  name: 'sizeFilter'
})
export class SizeFilterPipe implements PipeTransform {

  transform(items: Shirt[], filter?: string): Shirt[] {

    if (filter != null && filter !== '') {
      return items.filter((shirt) => shirt.size === filter);
    }

    return items;
  }

}
