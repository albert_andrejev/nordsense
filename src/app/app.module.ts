import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { StorageServiceModule } from 'ngx-webstorage-service';
import { ModalModule } from 'ngx-bootstrap';

import { AppComponent } from './app.component';

import { ShirtsListComponent } from './components/shirts-list/shirts-list.component';
import { AppRoutingModule } from './app-routing.module';
import { ShirtOverviewComponent } from './components/shirt-overview/shirt-overview.component';
import { SizeFilterPipe } from './pipes/size-filter.pipe';
import { ColourFilterPipe } from './pipes/colour-filter.pipe';
import { ShirtDetailsComponent } from './components/shirt-details/shirt-details.component';
import { BasketListComponent } from './components/basket-list/basket-list.component';

@NgModule({
  declarations: [
    AppComponent,
    ShirtsListComponent,
    ShirtOverviewComponent,
    SizeFilterPipe,
    ColourFilterPipe,
    ShirtDetailsComponent,
    BasketListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    StorageServiceModule,
    FormsModule,
    ModalModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
