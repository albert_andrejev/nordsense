import { EventService } from './event.service';

describe('EventService', () => {
  let service: EventService;
  let eventKey: string;

  beforeEach(() => {
    eventKey = 'someEventKey';

    service = new EventService();
  });

  it('Should subscribe to event', () => {
    const dumbFunc = () => {};
    service.subscribe(eventKey, dumbFunc);

    expect(service._handlers[eventKey][0]).toBe(dumbFunc);
  });

  it('should add multiple handlers to event', () => {
    service.subscribe(eventKey, () => {});
    service.subscribe(eventKey, () => {});
    service.subscribe(eventKey, () => {});

    expect(service._handlers[eventKey].length).toBe(3);
  });

  it('Should call handler on event', (done) => {
    const msgPayload = {
      action: 'add',
      payload: true
    };

    const dumbFunc = (msg) => {
      expect(msg).toEqual(msgPayload);
      done();
    };

    service.subscribe(eventKey, dumbFunc);

    service.event(eventKey, msgPayload);
  });
});
