import { Injectable } from '@angular/core';
import { AppEvent } from '../classes/app-event';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  public _handlers: any = {};

  constructor() { }

  subscribe(eventType: string, handler: (msg: AppEvent) => any) {
    let allTypeHandlers = this._handlers[eventType];
    if (!allTypeHandlers) {
      allTypeHandlers = [];
    }

    allTypeHandlers.push(handler);

    this._handlers[eventType] = allTypeHandlers;
  }

  event(eventType: string, msg: AppEvent) {
    const allTypeHandlers = this._handlers[eventType];

    if (allTypeHandlers) {
      allTypeHandlers.forEach(handler => {
        handler(msg);
      });
    }
  }
}
