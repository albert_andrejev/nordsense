import { Injectable, Inject } from '@angular/core';
import { EventService } from './event.service';
import { ConfigService } from './config.service';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { Basket } from '../classes/basket';
import { Shirt } from '../classes/shirt';
import { AppEvent } from '../classes/app-event';

@Injectable({
  providedIn: 'root'
})
export class PersistentStorageService {
  private basket: Basket[] = null;

  constructor(
    private event: EventService,
    private config: ConfigService,
    @Inject(LOCAL_STORAGE) private storage: StorageService
  ) { }

  load(): Basket[] {
    if (!this.basket) {
      this.basket = this.storage.get(this.config.storageKey()) || [];
    }

    return this.basket;
  }

  add(shirt: Shirt, amountToAdd: number) {
    const currentBasket = this.load();

    if (this.exist(shirt)) {
      const basketItem = currentBasket.find((inBasket) => inBasket.id === shirt.id);
      if (basketItem !== undefined) {
        basketItem.quantity += amountToAdd;
      }
    } else {
      currentBasket.push({
        id: shirt.id,
        name: shirt.name,
        price: shirt.price,
        quantity: amountToAdd
      });
    }

    this.storage.set(this.config.storageKey(), currentBasket);

    const payload: AppEvent = {
      action: 'add',
      payload: {
        amount: amountToAdd,
        id: shirt.id
      }
    };

    this.event.event(this.config.basketEvent(), payload);
  }


  remove(shirt: Shirt, amountToRemove: number) {
    const currentBasket = this.load();

    if (this.exist(shirt)) {
      const basketItemIdx = currentBasket.findIndex((inBasket) => inBasket.id === shirt.id);
      if (basketItemIdx !== -1) {
        const basketItem = currentBasket[basketItemIdx];
        if (basketItem.quantity > amountToRemove) {
          basketItem.quantity -= amountToRemove;
        } else {
          currentBasket.splice(basketItemIdx, 1);
        }
      }

      this.storage.set(this.config.storageKey(), currentBasket);

      const payload: AppEvent = {
        action: 'remove',
        payload: {
          amount: amountToRemove,
          id: shirt.id
        }
      };

      this.event.event(this.config.basketEvent(), payload);
    }
  }

  exist(shirt: Shirt): boolean {
    return this.load().find((inBasket) => inBasket.id === shirt.id) !== undefined;
  }
}
