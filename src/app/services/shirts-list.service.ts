import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Shirt } from '../classes/shirt';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class ShirtsListService {

  constructor(
    private http: HttpClient,
    private config: ConfigService
  ) { }

  getList(): Observable<Shirt[]> {
    return this.http.get<Shirt[]>(this.config.serviceEndpoint());
  }

  getShirt(shirtId: number): Observable<Shirt> {
    return this.getList().map((shirts) => {
      const selectedShirt = shirts.find((shirt) => shirt.id === shirtId);

      return selectedShirt;
    });
  }
}
