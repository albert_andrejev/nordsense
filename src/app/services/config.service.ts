import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor() { }

  serviceEndpoint() {
    return 'https://fast-eyrie-77564.herokuapp.com/';
  }

  storageKey() {
    return 'ShirtShopBasket';
  }

  basketEvent() {
    return 'BasketEvent';
  }

}
