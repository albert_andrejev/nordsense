import { TestBed } from '@angular/core/testing';

import { ShirtsListService } from './shirts-list.service';

describe('ShirtsListService', () => {
  let httpClientSpy: {
    get: jasmine.Spy
  };
  let configServiceSpy: {
    serviceEndpoint: jasmine.Spy
  };
  let observable: {
    subscribe: Function
  };

  let successFunc: Function;
  let errorFunc: Function;

  let serviceURL: string;

  let service: ShirtsListService;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    configServiceSpy = jasmine.createSpyObj('ConfigService', ['serviceEndpoint']);

    serviceURL = 'EndpointURL';
    configServiceSpy.serviceEndpoint.and.returnValue(serviceURL);

    observable = {
      subscribe: (success, error) => {
        successFunc = success;
        errorFunc = error;
      }
    };

    httpClientSpy.get.and.returnValue(observable);


    service = new ShirtsListService(<any> httpClientSpy, <any> configServiceSpy);
  });

  it('getList(). Should call service.', () => {
    service.getList();

    expect(httpClientSpy.get).toHaveBeenCalledWith(serviceURL);
  });

  it('getList(). Should return data from service.', (done) => {
    service.getList()
      .subscribe(
        (value) => {
          expect(value).toEqual(<any> true);
          done();
        }
      );

    successFunc(true);
  });
});
