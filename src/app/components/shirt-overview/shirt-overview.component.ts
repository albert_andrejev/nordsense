import { Component, OnInit, Input } from '@angular/core';
import { Shirt } from '../../classes/shirt';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shirt-overview',
  templateUrl: './shirt-overview.component.html',
  styleUrls: ['./shirt-overview.component.css']
})

export class ShirtOverviewComponent implements OnInit {
  @Input() shirts: Shirt[];
  @Input() size: String;
  @Input() colour: String;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  gotoShirt(shirtId: number) {
    this.router.navigate(['/shirts', shirtId]);
  }

}
