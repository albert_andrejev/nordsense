import { Component, OnInit } from '@angular/core';
import { PersistentStorageService } from '../../services/persistent-storage.service';
import { Basket } from '../../classes/basket';
import { Shirt } from '../../classes/shirt';

@Component({
  selector: 'app-basket-list',
  templateUrl: './basket-list.component.html',
  styleUrls: ['./basket-list.component.css']
})
export class BasketListComponent implements OnInit {
  public basket: Basket[] = [];
  private basketOrig: Basket[] = [];

  constructor(
    private basketStorage: PersistentStorageService
  ) { }

  ngOnInit() {
    this.basketOrig = this.basketStorage.load();
    this.basket = JSON.parse(JSON.stringify(this.basketOrig));
  }

  remove(item, i) {
    if (confirm('Are you sure want to remove this item?')) {
      this.basketStorage.remove(item, item.quantity);
      this.basket.splice(i, 1);

      return true;
    }

    return false;
  }

  getTotal(basket) {
    return basket.reduce((accu, item) => accu + item.price * item.quantity, 0);
  }

  onQtyChange(itemIdx, value) {
    const basketItem = this.basketOrig[itemIdx];
    const qtyChange = value - basketItem.quantity;
    if (qtyChange > 0) {
      this.basketStorage.add(<Shirt> basketItem, qtyChange);
    } else if (qtyChange < 0) {
      if (basketItem.quantity === Math.abs(qtyChange)) {
        if (!this.remove(basketItem, itemIdx)) {
          this.basket[itemIdx].quantity = 1;
        }
      } else {
        this.basketStorage.remove(<Shirt> basketItem, Math.abs(qtyChange));
      }
    }
  }
}
