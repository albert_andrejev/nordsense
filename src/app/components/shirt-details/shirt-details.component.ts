import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { ShirtsListService } from '../../services/shirts-list.service';
import { Shirt } from '../../classes/shirt';

import { Basket } from '../../classes/basket';
import { PersistentStorageService } from '../../services/persistent-storage.service';
import { EventService } from '../../services/event.service';
import { ConfigService } from '../../services/config.service';

@Component({
  selector: 'app-shirt-details',
  templateUrl: './shirt-details.component.html',
  styleUrls: ['./shirt-details.component.css']
})
export class ShirtDetailsComponent implements OnInit {
  public shirt: Shirt = undefined;
  public inBasket: Boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private config: ConfigService,
    private event: EventService,
    private shirtsService: ShirtsListService,
    private basketStorage: PersistentStorageService
  ) {
    this.event.subscribe(this.config.basketEvent(), (msg) => {
      if (msg.action === 'remove' && msg.payload.id === this.shirt.id) {
        this.inBasket = this.basketStorage.exist(this.shirt);
      }
    });
  }

  ngOnInit() {
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.shirtsService.getShirt(+params.get('id')))
    ).subscribe((shirt) => {
      this.shirt = shirt;
      this.inBasket = this.basketStorage.exist(this.shirt);
    });
  }

  backToList() {
    this.router.navigate(['/shirts']);
  }

  addToBasket() {
    this.basketStorage.add(this.shirt, 1);
    this.inBasket = true;
  }
}
