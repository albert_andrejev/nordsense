import { Component, OnInit } from '@angular/core';
import { ShirtsListService } from '../../services/shirts-list.service';
import { Shirt } from '../../classes/shirt';

@Component({
  selector: 'app-shirts-list',
  templateUrl: './shirts-list.component.html',
  styleUrls: ['./shirts-list.component.css']
})
export class ShirtsListComponent implements OnInit {
  public shirts: Shirt[];
  public sizes: Array<string>;
  public colours: Array<string>;
  public selectedSize: String;
  public selectedColour: String;
  constructor(private shirtsService: ShirtsListService) { }

  ngOnInit() {
    this.shirtsService.getList().subscribe((shirts: Shirt[]) => {
      this.shirts = shirts;

      const allSizes = shirts.map((shirt) => shirt.size).sort();
      this.sizes = Array.from(new Set(allSizes));

      const allColours = shirts.map((shirt) => shirt.colour).sort();
      this.colours = Array.from(new Set(allColours));
    });
  }

  onChangeSize(sizeValue) {
    this.selectedSize = sizeValue;
  }

  onChangeColour(colourValue) {
    this.selectedColour = colourValue;
  }
}
