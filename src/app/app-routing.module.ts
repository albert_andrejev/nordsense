import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShirtsListComponent } from './components/shirts-list/shirts-list.component';
import { ShirtDetailsComponent } from './components/shirt-details/shirt-details.component';

const routes: Routes = [
  { path: 'shirts', component: ShirtsListComponent },
  { path: 'shirts/:id', component: ShirtDetailsComponent },
  { path: '', redirectTo: '/shirts', pathMatch: 'full' },
];

@NgModule({
  exports: [ RouterModule ],
  imports: [
    RouterModule.forRoot(routes)
  ],
  declarations: []
})
export class AppRoutingModule { }
