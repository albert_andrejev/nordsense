import { Component, Inject, OnInit, TemplateRef } from '@angular/core';

import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { ConfigService } from './services/config.service';
import { Basket } from './classes/basket';
import { EventService } from './services/event.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public amountOfItems = 0;
  public backetModalRef: BsModalRef;

  title = 'NordsenseTask';

  constructor(
    private config: ConfigService,
    private event: EventService,
    private modalService: BsModalService,
    @Inject(LOCAL_STORAGE) private storage: StorageService
  ) {
    this.event.subscribe(this.config.basketEvent(), (msg) => {
      if (msg.action === 'add') {
        this.amountOfItems += msg.payload.amount;
      } else if (msg.action === 'remove') {
        this.amountOfItems -= msg.payload.amount;
      }
    });
  }

  ngOnInit(): void {
    const basket: Basket[] = this.storage.get(this.config.storageKey());

    if (basket) {
      basket.forEach((item) => {
        this.amountOfItems += item.quantity;
      });
    }
  }

  showBasket(template: TemplateRef<any>) {
    this.backetModalRef = this.modalService.show(template);
  }
}
