export class AppEvent {
    action: string;
    payload: any;
  }
