export class Basket {
    id: number;
    name: string;
    price: number;
    quantity: number;
  }
